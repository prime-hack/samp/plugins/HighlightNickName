#include "main.h"

#include <charconv>
#include <cstring>
#include <string>
#include <string_view>

#include "callfunc.hpp"

AsiPlugin::AsiPlugin() : SRDescent( nullptr ) {
	if ( SAMP::isR1() )
		renderString_.changeAddr( 0x63BE7 );
	else if ( SAMP::isR2() )
		renderString_.changeAddr( 0x63CB7 );
	else if ( SAMP::isR4() )
		renderString_.changeAddr( 0x67777 );
	else if ( SAMP::isDL() )
		renderString_.changeAddr( 0x67227 );
	renderString_.onBefore += std::tuple{ this, &AsiPlugin::renderString };
	renderString_.install( 8, 0, false );
}

AsiPlugin::~AsiPlugin() {
	if ( base != nullptr ) SAMP::Base::DeleteInstance();
	if ( font != nullptr ) SAMP::Fonts::DeleteInstance();
	if ( render != nullptr ) g_class.DirectX->d3d9_releaseRender( render );
}

void AsiPlugin::renderString( char *&text, int &length, long *&rect ) {
	if ( base == nullptr ) base = SAMP::Base::Instance();
	if ( font == nullptr ) font = SAMP::Fonts::Instance();
	if ( render == nullptr ) render = g_class.DirectX->d3d9_createRender();

	if ( base->Pools() == nullptr ) return;

	auto pool = base->Pools()->PlayerPool();
	if ( pool == nullptr ) return;

	auto nick = pool->localPlayerNick();
	auto size = font->measureText( nick );

	int color = 0x80FFFFFF;
	switch ( SAMP::Version() ) {
		case SAMP::eVerCode::R1:
			color = CallFunc::ccall<int>( SAMP::Library() + 0x3D90 );
			break;
		case SAMP::eVerCode::R2:
			[[fallthrough]];
		case SAMP::eVerCode::R3:
			color = CallFunc::ccall<int>( SAMP::Library() + 0x3DA0 );
			break;
		case SAMP::eVerCode::R4:
			color = CallFunc::ccall<int>( SAMP::Library() + 0x3F10 );
			break;
		case SAMP::eVerCode::DL:
			color = CallFunc::ccall<int>( SAMP::Library() + 0x3E20 );
			break;
		default:
			break;
	}
//	color = 0x80000000 | ( 0x00FFFFFF - ( color & 0x00FFFFFF ) ); // Inverted color
	color = 0x80000000 | ( color & 0x00FFFFFF );

	auto text_ = text;
	while ( true ) {
		auto nick_at = std::strstr( text_, nick.c_str() );
		if ( nick_at == nullptr ) break;

		int offset = 0;
		if ( nick_at != text ) {
			*nick_at = '\0';
			offset = font->measureText( text ).width;
			*nick_at = nick.front();
		}

		render->D3DBox( ( rect[0] - 2 ) + offset, rect[1], size.width + 2, size.height, color );
		text_ = &nick_at[nick.length()];
	}
}
