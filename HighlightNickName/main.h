#ifndef MAIN_H
#define MAIN_H

#include "SAMP/Base.h"
#include "loader/loader.h"
#include <SRDescent/SRDescent.h>

#include "d3drender.h"
#include "SRHook.hpp"
#include "samp.hpp"

class AsiPlugin : public SRDescent {
	SRHook::Hook<char *, int, long *> renderString_{ 0x67037, 5, "samp" };

public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();

protected:
	SAMP::Base *base = nullptr;
	SAMP::Fonts *font = nullptr;
	CD3DRender *render = nullptr;
	void renderString( char *&text, int &length, long *&rect );
};

#endif // MAIN_H
